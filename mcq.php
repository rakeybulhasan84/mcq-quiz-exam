<?php
/**
 * Plugin Name:       Multiple Choice Exam
 * Description:       Handle the multiple choice question exam management with this plugin.
 * Version:           1.10.3
 * Author:            Muhammad Rakeybul Hasan
 * Text Domain:       mcq
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'MCQ_VERSION', '1.10.3' );

define( 'MCQ_FILE', __FILE__ );
define( 'MCQ_PATH', dirname( MCQ_FILE ) );
wp_enqueue_script( 'mcq-script', plugin_dir_url( __FILE__ ) . 'assets/js/script.js', array( 'jquery' ), MCQ_VERSION, true );

function multiple_choice_question() {

    $labels = array(
        'name'                  => _x( 'MCQ', 'Post Type General Name', 'mcq' ),
        'singular_name'         => _x( 'MCQ', 'Post Type Singular Name', 'mcq' ),
        'menu_name'             => __( 'MCQ', 'mcq' ),
        'name_admin_bar'        => __( 'MCQ', 'mcq' ),
        'archives'              => __( 'Item Archives', 'mcq' ),
        'attributes'            => __( 'Item Attributes', 'mcq' ),
        'parent_item_colon'     => __( 'Parent Item:', 'mcq' ),
        'all_items'             => __( 'All Questions', 'mcq' ),
        'add_new_item'          => __( 'Add New Question', 'mcq' ),
        'add_new'               => __( 'Add New Question', 'mcq' ),
        'new_item'              => __( 'New Question', 'mcq' ),
        'edit_item'             => __( 'Edit Question', 'mcq' ),
        'update_item'           => __( 'Update Question', 'mcq' ),
        'view_item'             => __( 'View Question', 'mcq' ),
        'view_items'            => __( 'View Items', 'mcq' ),
        'search_items'          => __( 'Search Item', 'mcq' ),
        'not_found'             => __( 'Not found', 'mcq' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'mcq' ),
        'featured_image'        => __( 'Featured Image', 'mcq' ),
        'set_featured_image'    => __( 'Set featured image', 'mcq' ),
        'remove_featured_image' => __( 'Remove featured image', 'mcq' ),
        'use_featured_image'    => __( 'Use as featured image', 'mcq' ),
        'insert_into_item'      => __( 'Insert into item', 'mcq' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'mcq' ),
        'items_list'            => __( 'Items list', 'mcq' ),
        'items_list_navigation' => __( 'Items list navigation', 'mcq' ),
        'filter_items_list'     => __( 'Filter items list', 'mcq' ),
    );
    $args = array(
        'label'                 => __( 'MCQ', 'mcq' ),
        'description'           => __( 'Multiple Choice Question', 'mcq' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'mcq_post_type', $args );

}
add_action( 'init', 'multiple_choice_question', 0 );

function change_title_placeholder( $title ){
    $screen = get_current_screen();

    if  ( 'mcq_post_type' == $screen->post_type ) {
        $title = 'Enter Question';
    }

    return $title;
}

add_filter( 'enter_title_here', 'change_title_placeholder' );

function add_mcq_custom_meta_box() {
    add_meta_box(
        'mcq_meta_box', // $id
        'Question Options And Answer', // $title
        'show_options_fields_meta_box', // $callback
        'mcq_post_type', // $screen
        'normal', // $context
        'high' // $priority
    );
}
add_action( 'add_meta_boxes', 'add_mcq_custom_meta_box' );

function show_options_fields_meta_box() {
    global $post;
    $options = get_post_meta( $post->ID, '_choice_option', true );
    $answer = get_post_meta( $post->ID, '_choice_answer', true );
    $_question_type = get_post_meta( $post->ID, '_question_type', true );

    $optionsArray=json_decode($options,true);
    ?>

    <input type="hidden" name="mcq_meta_box_nonce" value="<?php echo wp_create_nonce( basename(__FILE__) ); ?>">
    <h4>Question Options</h4>
    <?php for($i=1; $i<=4;$i++){?>
    <p>
        <label>Option <?php echo $i;?></label>
        <input type="text" name="_choice_option[<?php echo $i;?>]" class="regular-text" value="<?php echo $optionsArray[$i];?>">
    </p>
    <?php } ?>

    <h4>Question Answer</h4>

    <p>
        <label>Answer</label>
        <input type="text" name="_choice_answer" class="regular-text" value="<?php echo $answer;?>">
    </p>
    <h4>Question Type</h4>

    <p>
        <label>Type</label>

        <select name="_question_type" id="">
            <option value="single" <?php echo $_question_type=='single'?'selected="selected"':''?>>Single</option>
            <option value="multiple" <?php echo $_question_type=='multiple'?'selected="selected"':''?>>Multiple</option>
        </select>
    </p>

    <!-- All fields will go here -->

<?php }



function save_mcq_meta_data($post_id, $post) {

    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !wp_verify_nonce( $_POST['mcq_meta_box_nonce'], basename(__FILE__) ) ) {
//       var_dump($post_id);die;
        return $post_id;
    }


    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;

    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.

    $mcq_meta_data['_choice_option'] = $_POST['_choice_option'];
    $mcq_meta_data['_choice_answer'] = $_POST['_choice_answer'];
    $mcq_meta_data['_question_type'] = $_POST['_question_type'];
//var_dump($mcq_meta_data);die;
    // Add values of $important_links_meta as custom fields

    foreach ($mcq_meta_data as $key => $value) { // Cycle through the $important_links_meta array!

        if( $post->post_type == 'revision' ) return; // Don't store custom data twice
        if($key=='_choice_option'){
            $value= json_encode($value);
        }else{
            $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
        }
        if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
            update_post_meta($post->ID, $key, $value);
        } else { // If the custom field doesn't have a value
            add_post_meta($post->ID, $key, $value);
        }
        if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }

}

add_action('save_post', 'save_mcq_meta_data', 1, 2);

// function that runs when shortcode is called
function mcq_quiz_shortcode() {
    $message='<div class="row">';
    $args = array(
        'post_status' => 'publish',
        'post_type' => 'mcq_post_type',
    );
    $questionNo=1;

    $loop = new WP_Query( $args );

    while ( $loop->have_posts() ) : $loop->the_post();
    $message.='<div class="question_section">';
        $message.= $questionNo.'. '. get_the_title();
        $message.='<br>';
        $options = get_post_meta( get_the_ID(), '_choice_option', true );
        $question_type = get_post_meta( get_the_ID(), '_question_type', true );
        $optionsArray=json_decode($options,true);
        foreach ($optionsArray as $key=>$value){
            if($question_type=='multiple'){
                $message.='<input data-post-id="'.get_the_ID().'" type="checkbox" name="_correct_answer" class="correct_answer" value="'.$key.'"> '. $value;
            }else{
                $message.='<input data-post-id="'.get_the_ID().'" type="radio" name="_correct_answer" class="correct_answer" value="'.$key.'"> '. $value;
            }
            $message.='<br>';
        }
        $message.='<br>';
        $questionNo++;
        $message.='</div>';
    endwhile;

    $message.='</div>';
// Things that you want to do.


// Output needs to be return
    return $message;
}
// register shortcode
add_shortcode('mcqquiz', 'mcq_quiz_shortcode');
